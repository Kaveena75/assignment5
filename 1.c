#include<stdio.h>


int Profit (int Ticketprice)
{
    return Revenue(Ticketprice)- Cost(Ticketprice);
}

int Revenue (int Ticketprice)
{
    return Attendees(Ticketprice) * Ticketprice;
}

int Cost (int Ticketprice)
{
    return 500 + (3 * Attendees(Ticketprice));
}

int Attendees (int Ticketprice)
{
    return 120 + (20 * (15 - Ticketprice)/5);
}

int main()
{
    int Ticketprice,maxprofit,bestprice;
    printf("Ticket price\t No. of Attendees\t profit\n");
    for (Ticketprice=0; Ticketprice<50; Ticketprice = Ticketprice + 5)
    {
        printf("Rs.%d\t\t %d\t\t %d\n", Ticketprice, Attendees(Ticketprice), Profit(Ticketprice));

    }
    for (Ticketprice=0; Ticketprice<50; Ticketprice=Ticketprice + 5)
    {
        if (maxprofit < Profit(Ticketprice))
        {
            maxprofit = Profit(Ticketprice);
            bestprice = Ticketprice;
        }
    }
    printf ("\nThe most profitable ticket price is Rs. %d.", bestprice);
    return 0;
}
